import axios from 'axios'
import moment from 'moment';

//const INSTRUCTOR = 'in28minutes'
const API_URL = 'http://localhost:8888'
const ROOMS_API_URL = `${API_URL}/users/`

export const retrieveReservations = (userId) => {
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': '*'
    }
    const url = ROOMS_API_URL + + userId + "/reservations";
    return axios.get(url, {
        userId
    }, headers);
}
