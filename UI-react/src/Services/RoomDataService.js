import axios from 'axios'
import moment from 'moment';

//const INSTRUCTOR = 'in28minutes'
const API_URL = 'http://localhost:8888'
let errors = {};
export const reserveRoom = (userId, checkin, checkout, roomId) => {
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': '*'
    }
    const url = API_URL + '/users/' + localStorage.getItem('userId')+ '/reservations';//"/users/' 'reservations";
    axios.post(url, {
        userId,
        checkin: new moment(checkin, "YYYY-MM-DD").add(1, "days").format("DD-MM-YYYY"),
        checkout: new moment(checkout, "YYYY-MM-DD").add(1, "days").format("DD-MM-YYYY"),
        rentedRoomsIds: [roomId]
    }, headers).then(response =>{
         console.log(response.data); 
         alert('Your reservation was successfully done!')
         errors['WrongCredentials'] = null;})
    .catch(error => {
        if (error.status === 400) {
          console.log(
            'Please enter valid dates'
          );
          errors['WrongCredentials'] =
          'Please enter valid dates';
        //   errors['WrongCredentials'] =
        //     'Your Username or Password is incorrect. Please try again!';
        //   this.setState({ isError: true });
        // } else 
            } else {
               
                alert(  'Please enter valid dates');
            }
        });

    };


