import React, { useEffect } from "react";
import { scrollTo } from "utils";
import TopBar from "./sections/TopBar9";
import Portfolio1 from "./sections/ReservationsPortofolio";

import Footer1 from "./sections/Footer1";

class ReservationsLanding extends React.Component {
 
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
    }
  }

  render(){
    return (
      <div className="landing" >
        <TopBar />
        <Portfolio1 />
        <Footer1 />
      </div>
    );
  }
};

export default ReservationsLanding;
