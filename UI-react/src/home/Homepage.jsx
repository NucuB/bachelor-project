import React from "react";
import Footer2 from "./sections/Footer2";
import TopBar9 from "./sections/TopBar9";
import Intro9 from "./sections/Intro9";
import Contact2 from "./sections/Contact2";
import Booking1 from "./sections/Booking1";
import Gallery2 from "./sections/Gallery2";
import HotelTour from "./sections/HotelTour";
import Services10 from "./sections/Services10";
import Blog1 from "./sections/Blog1";
import Testimonial8 from "./sections/Testimonial8";
import Reservations from "./sections/Reservations";
import { scrollTo } from "utils";
import axios from 'axios';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      price: null,
      message: null
    }
  }
  savePost = (message) => {
    // alert(index);
    this.setState({
      message: message
    })
     let userId = localStorage.getItem('userId');
     const API_URL = 'http://localhost:8888'
  
     const headers = {
       'Content-Type': 'application/json',
       'Authorization': '*'
   }
   const url = API_URL + '/users/' + localStorage.getItem('userId')+ '/posts';//"/users/' 'reservations";
   axios.post(url, {
      userId,
      message
   }, headers).then(response => console.log(response.data));
   }


  clickRoom = (ind) => {
     if (localStorage.getItem('userId') == null)
     alert('You need to be logged in order to perform a reservation')
    else {
      this.setState({
        index: ind
      })
    }
    
  }

  setPrice = (pr) => {
    this.setState({
      price: pr
    })
    console.log(pr);
  }

  handleChange = (event) => {
    console.log(event.target.id);
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  render() {
    return (
      <div className="homepage">
        <TopBar9 />
        <Intro9 />
        <Booking1 index={this.state.index} price ={this.state.price} setPrice={this.state.price}/>
        <Gallery2 clickRoom={this.clickRoom} price={this.state.price} setPrice={this.setPrice} />
        <HotelTour />
        <Services10 />
        <Blog1 />
        <Testimonial8 />
        <Contact2  savePost={this.savePost} handleChange = {this.handleChange} />
        <Footer2 />
        <Reservations />
      </div>
    );
  }
};

export default HomePage;
