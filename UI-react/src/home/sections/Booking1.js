import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { reserveRoom } from "../../Services/RoomDataService";
import clsx from "clsx";
import { Button, Card, Grid, TextField, Fab, Icon } from "@material-ui/core";
import moment from 'moment';
import ScrollTo from "home/common/ScrollTo";
import { scrollTo } from "utils";



const useStyles = theme => ({
  section: {
    background: "rgba(var(--primary),0.05)",
  },
});


class Booking1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkIn: null,
      checkOut: null,
      price: null,
      numberOfNights: null,
    }
  }

  save = (index) => {
    let userId = localStorage.getItem('userId');
    let roomId = this.props.index;
    console.log(this.state.checkIn)
    console.log(this.state.checkOut)
    reserveRoom(userId, this.state.checkIn, this.state.checkOut, roomId);
  }

  handleChange = (event) => {
    console.log(event.target.id);
    
    this.setState({
      [event.target.id]: event.target.value,
    })
  
  }
  render() {
    const classes = this.props;
    console.log("State = ", this.state);
    let totalPrice = 0;
    if(this.state.checkIn !== null && this.state.checkOut !== null) {
      console.log('ICI', this.state.checkIn, this.state.checkOut);
      let data = moment(this.state.checkOut).diff(moment(this.state.checkIn), 'days');
      console.log(data);
      totalPrice = Number(data);
      //this.setState({numberOfNights : data});
    }
    return (
      <section className={clsx("section", classes.section)} id="booking1">
        <div className="container">
        <div className="flex flex-wrap">
           
           <Button onClick={() => { scrollTo(document.querySelector(".scrollable-content"), "gallery2");}}
               variant="contained"
               color="primary"
               className="mr-4 border-radius-8"
             >
               Book Now
             </Button>
            
           
           </div>
          <Card elevation={0} className="p-12 border-radius-8">
           
            <div className="flex flex-wrap items-center ml--2">
              <TextField id="checkIn" type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={this.handleChange} label="Check In" variant="outlined" className="m-2" />
              <TextField id="checkOut" type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={this.handleChange} label="Check Out" variant="outlined" className="m-2" />
              <TextField id="price" onChange={this.handleChange} variant="outlined" label="Price" className="m-2" value={(this.props.price ? this.props.price * totalPrice : 0) + '€'} />
            
              <Fab onClick={() => this.save(this.props.index)} variant="round" color="primary" size="medium" className="m-2">
                <Icon>navigate_next</Icon>
              </Fab>
            </div>
          </Card>
        </div>
      </section>
    );
  }
};

export default withStyles(useStyles)(Booking1);
