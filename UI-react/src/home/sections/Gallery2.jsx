import React from "react";
import { Grid, Button, Divider, Icon } from "@material-ui/core";
import { makeStyles, lighten } from "@material-ui/core/styles";
import clsx from "clsx";
import axios from 'axios';
import ScrollTo from "home/common/ScrollTo";
import { scrollTo } from "utils";

const useStyles = makeStyles(({ palette, ...theme }) => ({
  cardHolder: {
    position: "relative",
    borderRadius: 8,
    overflow: "hidden",
    "&:hover $cardOverlay": {
      opacity: 1,
    },
  },
  cardOverlay: {
    padding: "0px 1rem",
    transition: "all 250ms ease-in-out",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    position: "absolute",
    borderRadius: 8,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0,
    color: palette.primary.contrastText,
    background: "rgba(0,0,0,0.67)",
    zIndex: 5,
  },
  cardTitle: {
    borderBottom: "1px solid rgba(255,255,255,0.87)",
  },
}));




const Gallery2 = (props) => {
  const classes = useStyles();
  let [responseData, setResponseData] = React.useState('');
  const [isClosed, setIsClosed] = React.useState(true);
  var priceRoom = 0;
  var rooms = null;
  const close = () => {
    setIsClosed(false);
  };

  const fetchData = React.useCallback(() => {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': '*'
  }
    axios({
      "method": "GET",
      "url": 'http://localhost:8888/rooms',
      "withCredentials": false,
      "headers": headers

    })
      .then((response) => {
        localStorage.setItem('rooms', JSON.stringify(response.data));
        //console.log(JSON.parse(localStorage.getItem('rooms')));
        rooms = JSON.parse(localStorage.getItem('rooms'));
        priceRoom = rooms[0].price;
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])
  React.useEffect(() => {
    fetchData()
  }, [fetchData])

  /*return (
    <div className="App">
      <header className="App-header">
        <h1>
          Fetching Data with React Hooks
        </h1>
        <button type='button' onClick={fetchData}>Click for Data</button>
      </header>
      <main>
        {responseData &&
          <blockquote>
            "{responseData && responseData.content}"
            <small>{responseData && responseData.originator && responseData.originator.name}</small>
          </blockquote>
        }
        </main>
     
    </div>
  );*/

  return (
    <section className="section" id="gallery2">
      <div className="container">
        <div className="mb-16 text-center">
          <h1 className="font-normal text-44 mt-0">Rooms & Suits</h1>
          <p className="max-w-400 mx-auto">
           Islands Resort Rooms Gallery
          </p>
        </div>

        
        <Grid container spacing={3}>
          {JSON.parse(localStorage.getItem('rooms')).map((item, ind) => (

            <Grid key={ind} item lg={4} md={4} sm={4} xs={12}>

              <div onClick={() => { setIsClosed(true); props.clickRoom(item.roomId); props.setPrice(item.price); scrollTo(document.querySelector(".scrollable-content"), "booking1");}} className={classes.cardHolder}>
                <img
                  className="w-full block"
                  src={`/assets/images/room-${item.roomId}.jpg`}
                  alt="random"


                // onClick =
                // onClick={() => {
                //   axios({
                //     "method": "GET",
                //     "url": 'http:/localhost:8888/users/?id=12',

                //   })
                //   .then((response) => {
                //     setResponseData(response.data)
                //   })
                //   .catch((error) => {
                //     console.log(error)
                //   })
                // }}
                // React.useEffect(() => {
                //   fetchData()
                // }, [fetchData])}}

                />
                <div className={classes.cardOverlay}>
                  <div>
                    <h5
                      className={clsx(
                        "m-0 mb-2 pb-2 font-medium inline-block",
                        classes.cardTitle
                      )}
                    >
                      Regular Room
                    </h5>
                  </div>
                  <div className="flex items-center mb-2">
                    <Icon className="text-inherit">king_bed</Icon>
                    <span className="ml-2 mr-4">1 Bed King</span>

                    <Icon className="text-inherit">bathtub</Icon>
                    <span className="ml-2 mr-4">1 Bathroom</span>
                  </div>
                      <span className="mb-3">{item.price}€/Night</span>
                </div>
              </div>
            </Grid>
          ))}
        </Grid>
      </div>
    </section>
  );
};

export default Gallery2;
