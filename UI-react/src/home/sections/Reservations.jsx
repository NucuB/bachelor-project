import React, { useState } from "react";
import { Grid, Icon, IconButton } from "@material-ui/core";
import { makeStyles , withStyles} from "@material-ui/core/styles";
import { retrieveReservations } from "../../Services/ReservationsService";
import { Card, TextField, Fab} from "@material-ui/core";
import {
  Wifi,
  Spa,
  SportsHandball,
  RestaurantMenu,
  AirlineSeatIndividualSuite,
  BeachAccess,
} from "@material-ui/icons";
import { render } from "react-dom";
import axios from 'axios';
import { NavLink } from "react-router-dom";

const useStyles = theme => ({
  iconWrapper: {
    position: "relative",
    display: "inline-block",
    zIndex: 2,
    "&::before": {
      content: '" "',
      position: "absolute",
      height: "78px",
      width: "42px",
      borderRadius: "300px",
      transform: "rotate(40deg)",
      top: "calc(50% - 42px)",
      left: 10,
      background: "rgba(var(--primary),0.15)",
      zIndex: -1,
    },
  },
  doneIcon: {
    position: "absolute",
    bottom: 0,
    right: "calc(50% - 32px)",
    zIndex: 3,
  },
});

class Reservations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reservations: null,
      price: null,
    }
  }

  componentDidMount(){
    var userId = localStorage.getItem('userId');
    retrieveReservations(userId).then(response => {
      console.log(response.data);
     
      //this.setState('reservations', response.data);
    });

    
  }
    /*
  let res = {};

  const getReservations = () => {
        alert("reservation done");
        let userId = 12;
        retrieveReservations(userId).then(response => {
            console.log(response.data);
            setReservationsToVar(response.data);
        });
  };

  const setReservationsToVar = (data) => {
    let res = data;
    if(res != null){
        console.log("res  ", res[0]);
        return res.map(reserv => {
            return (
              <tr>
                <td>{reserv.reservationId}</td>
                <td>{reserv.checkin}</td>
                <td>{reserv.checkout}</td> 
              </tr>
            )
          }) 
    }
  }

  const renderTable = () => {
    return  (
        <div>
        {setReservationsToVar()}
        </div>
    )
  }
  */
  render() {
    return (
      <section className="section" id="reservations">
        <div className="container">
          <div className="mb-16 text-center">
            <h1 className="font-normal text-44 mt-0">Your reservations</h1>
            <p className="max-w-400 mx-auto">
              Click to see your reservations
            </p>
           
              <Fab onClick={() => window.location = "http://localhost:3000/reservations"} variant="round" color="primary" size="medium" className="m-2">
                  <Icon>navigate_next</Icon>
                </Fab>
            
          </div>
        <div>
          {//renderTable()
          }
          </div>  
        </div>
      </section>
    );
  }
};

export default withStyles(useStyles)(Reservations);
