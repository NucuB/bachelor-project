import React from "react";
import { Grid, Card, TextField, Button } from "@material-ui/core";
import axios from 'axios';
import { useState } from "react";
/*savePost = () => {
  // alert(index);
   let userId = localStorage.getItem('userId');
   let message = '';
   const API_URL = 'http://localhost:8888'

   const headers = {
     'Content-Type': 'application/json',
     'Authorization': '*'
 }
 const url = API_URL + '/users/' + localStorage.getItem('userId')+ '/posts';//"/users/' 'reservations";
 axios.post(url, {
     userId,
    message
 }, headers).then(response => console.log(response.data));
 }*/

const Contact2 = (props) => {
  const [message, setMessage] = useState('');

  return (
    <section className="section" id="contact2">
      <div className="container">
        <h1 className="mt-0 mb-16 font-normal text-44">Get in touch with us</h1>
        <Grid container>
          <Grid item>
            <Card className="py-10 px-6" elevation={3}>
              <form>
                <TextField  id="message" onChange={(event) => { setMessage (event.target.value) }}   
                  className="mb-4"
                  label="Message"
                  placeholder="Message"
                  size="small"
                  multiline
                  rows={8}
                  variant="outlined"
                  fullWidth
                />
                <Button onClick={() => props.savePost(message)} className="w-full" variant="contained" color="primary">
                  SEND MESSAGE
                </Button>
              </form>
            </Card>
          </Grid>
        </Grid>
      </div>
    </section>
  );
};

export default Contact2;
