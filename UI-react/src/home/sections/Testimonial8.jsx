import React from "react";
import { Avatar, Icon, useMediaQuery } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Carousel from "../common/Carousel";
import TwitterIcon from "../common/icons/TwitterIcon";
import FacebookIcon from "../common/icons/FacebookIcon";
import clsx from "clsx";

const useStyles = makeStyles(({ palette, ...theme }) => ({
  section: {
    background: `linear-gradient(to left, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.7) 100%), 
    url('./assets/images/scene-2.jpg')`,
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
  },
  card: {
    maxWidth: 700,
  },
  review: {
    transform: "skewY(0.75deg)",
    transformOrigin: "bottom left",
  },
}));

const Testimonial8 = () => {
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("xs"));

  const testimonialList = [
    {
      companyLogoUrl: "./assets/images/mock-logo-1.png",
      comment: `"Great times"`,
      icon: TwitterIcon,
      user: {
        imageUrl: "./assets/images/face-1.png",
        name: "Ken Orwen"
        
      },
    },
    {
      companyLogoUrl: "./assets/images/mock-logo-2.png",
      comment: `"Great times"`,
      icon: FacebookIcon,
      user: {
        imageUrl: "./assets/images/face-2.png",
        name: "User1"
        
      },
    }
  ];

  return (
    <div className={clsx("section", classes.section)} id="testimonial8">
      <div className="container text-center">
        <h1 className="font-normal text-44 mt-0 text-white mx-auto mb-16">
          What our guests have to say
        </h1>
        <Carousel
          bulletColor={theme.palette.primary.contrastText}
          slidesPerView={1}
          spacing={0}
          navigation={false}
          paginationClass="mt-16"
        >
          {testimonialList.map((testimonial, index) => (
            <div className={clsx("mx-auto", classes.card)} key={index}>
              <div
                className={clsx({
                  "flex justify-center": true,
                  "flex-wrap": isMobile,
                })}
              >
                <Avatar
                  className="w-108 h-108"
                  src={testimonial.user.imageUrl}
                  alt="user"
                />
                <p
                  className={clsx({
                    "text-white": true,
                    "text-left my-0 ml-8": !isMobile,
                    [classes.review]: true,
                  })}
                >
                 "Great places, great times"
                </p>
              </div>
              <div className="flex flex-wrap mt-4 justify-center mb-2">
                {[1, 2, 3, 4, 5].map((i) => (
                  <Icon
                    key={i}
                    className="mx-1"
                    fontSize="small"
                    color="secondary"
                  >
                    start
                  </Icon>
                ))}
              </div>
              <h5 className="inline-block m-0 font-medium text-white">
                {testimonial.user.name}, {testimonial.user.designation}
              </h5>
            </div>
          ))}
        </Carousel>
      </div>
    </div>
  );
};

export default Testimonial8;
