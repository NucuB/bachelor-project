import React from "react";
import Card from "@material-ui/core/Card";

import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import { Button, Icon } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import axios from 'axios';

const useStyles = makeStyles(({ palette, ...theme }) => ({}));

class Portfolio1 extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      portofolioList : '',
    }
  }

  /*
  <Grid item md={4} sm={4} key={index}>
        <Card className="relative h-full card" key={index}>
          <img
            className="w-full"
            src={`/assets/images/room-${index}.jpg`}
            alt="portfolio"
          />
          <div className="flex-column justify-between">
            <div className="px-4 pt-4">
              <h5 className="m-0 text-16 font-bold">{}</h5>
              <p className="mb-4">{}</p>
              <Divider />
            </div>
            <div className="px-4 py-2">
              <IconButton>
                <Icon>link</Icon>
              </IconButton>
              <IconButton>
                <Icon>share</Icon>
              </IconButton>
            </div>
          </div>
        </Card>
      </Grid>
  */
 
  componentDidMount(){
    const API_URL = 'http://localhost:8888'
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': '*'
    }
    const url = API_URL + "/users/" + localStorage.getItem('userId') + "/reservations";
    
    axios.get(url, headers).then(response => { 
      this.setState({portofolioList : response.data});
      console.log(response.data);
    })
    .catch(error => {
        console.error('There was an error!', error);
    });
  }
    
  render(){

    const { portofolioList } = this.state;

    let reservationsList = 
    portofolioList.length > 0 && portofolioList.map((room, index) => {
        return(
          <Grid item md={4} sm={4} key={index}>
          <Card className="relative h-full card" key={index}>
            <img
              className="w-full"
              src={`/assets/images/room-${index+1}.jpg`}
              alt="portfolio"
            />
            <div className="flex-column justify-between">
              <div className="px-4 pt-4">
                <h5 className="m-0 text-16 font-bold">{room.checkin} - {room.checkout}</h5>
                
                <p className="mb-4">{room.price}</p>
                
                <Divider />
              </div>
              
            </div>
          </Card>
        </Grid>
        );
      })
    return (
      <section className="section" id="portfolio1">
        <div className="container">
          <div className="section__header">
            <h2>Reservations</h2>
            <p>
              
            </p>
          </div>
          <Grid container spacing={3}>
            {reservationsList}
          </Grid>

          <div className="text-center pt-9">
          
          </div>
        </div>
      </section>
    );
  }
};

export default (withStyles)(useStyles)(Portfolio1);
