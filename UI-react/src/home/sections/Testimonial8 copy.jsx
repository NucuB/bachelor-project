import React from "react";
import { Avatar, Icon, useMediaQuery } from "@material-ui/core";
import { makeStyles, useTheme, withStyles, ThemeProvider } from "@material-ui/core/styles";
import Carousel from "../common/Carousel";
import TwitterIcon from "../common/icons/TwitterIcon";
import FacebookIcon from "../common/icons/FacebookIcon";
import clsx from "clsx";
import axios from 'axios';
import { render } from "react-dom";

const useStyles = makeStyles(({ palette, ...theme }) => ({
  section: {
    background: `linear-gradient(to left, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.7) 100%), 
    url('./assets/images/scene-2.jpg')`,
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
  },
  card: {
    maxWidth: 700,
  },
  review: {
    transform: "skewY(0.75deg)",
    transformOrigin: "bottom left",
  },
}));

const theme = useTheme();

class Testimonial8 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
    
      isMobile: null,
      userId: 0,
      posts: []
    }
  }

  componentDidMount() {
   // this.state.setState({classes: useStyles()});
   // this.state.setState({theme: useTheme}),
  //  this.state.setState({isMobile: useMediaQuery(this.state.theme.breakpoints.down("xs"))}),
    //this.state.setState({userId: localStorage.getItem('userId')}),
    //this.state.setState({posts: this.getPosts()})
    let userId = localStorage.getItem('userId');
     const API_URL = 'http://localhost:8888'
  
     const headers = {
       'Content-Type': 'application/json',
       'Authorization': '*'
   }
   const url = API_URL + '/users/' + localStorage.getItem('userId')+ '/posts';//"/users/' 'reservations";
   axios.get(url).then(response => 
    {
      this.setState( {posts: response.data});
      console.log(response.data);
    });
   }
  
  
  /*getPosts = async (userId) => {
    const API_URL = 'http://localhost:8888'
    const ROOMS_API_URL = `${API_URL}/users/`
      const headers = {
          'Content-Type': 'application/json',
          'Authorization': '*'
      }
      const url = ROOMS_API_URL + + userId + "/posts";
      return await axios.get(url, {
          userId
      }, headers);
  }  */
  
    /* [
      {
        companyLogoUrl: "./assets/images/mock-logo-1.png",
        comment: `""`,
        icon: TwitterIcon,
        user: {
          imageUrl: "./assets/images/face-1.png",
          name: "John Doe",
          designation: "Product Manager",
        },
      },
      {
        companyLogoUrl: "./assets/images/mock-logo-2.png",
        comment: `"Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Fugit modi voluptas vero iusto fuga quos totam
            eius, atis magnam tempora doloribus ducimus dolorem
            culpa animi beatae tenetur! Sapiente, quia tempora."`,
        icon: FacebookIcon,
        user: {
          imageUrl: "./assets/images/face-2.png",
          name: "Adam Smith",
          designation: "CEO",
        },
      },
      {
        companyLogoUrl: "./assets/images/mock-logo-3.png",
        comment: `"Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Fugit modi voluptas vero iusto fuga quos totam
            eius, atis magnam tempora doloribus ducimus dolorem
            culpa animi beatae tenetur! Sapiente, quia tempora."`,
        icon: TwitterIcon,
        user: {
          imageUrl: "./assets/images/face-3.png",
          name: "John White",
          designation: "Software Engineer",
        },
      },
      {
        companyLogoUrl: "./assets/images/mock-logo-2.png",
        comment: `"Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Fugit modi voluptas vero iusto fuga quos totam
            eius, atis magnam tempora doloribus ducimus dolorem
            culpa animi beatae tenetur! Sapiente, quia tempora."`,
        icon: FacebookIcon,
        user: {
          imageUrl: "./assets/images/face-2.png",
          name: "Adam Smith",
          designation: "CEO",
        },
      },
      {
        companyLogoUrl: "./assets/images/mock-logo-3.png",
        comment: `"Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Fugit modi voluptas vero iusto fuga quos totam
            eius, atis magnam tempora doloribus ducimus dolorem
            culpa animi beatae tenetur! Sapiente, quia tempora."`,
        icon: TwitterIcon,
        user: {
          imageUrl: "./assets/images/face-3.png",
          name: "John White",
          designation: "Software Engineer",
        },
      },
      {
        companyLogoUrl: "./assets/images/mock-logo-4.png",
        comment: `"Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Fugit modi voluptas vero iusto fuga quos totam
            eius, atis magnam tempora doloribus ducimus dolorem
            culpa animi beatae tenetur! Sapiente, quia tempora."`,
        icon: FacebookIcon,
        user: {
          imageUrl: "./assets/images/face-4.png",
          name: "Jessica Hiche",
          designation: "CEO",
        },
      },
    ];*/
  render () {
    const {classes} = this.props;
    const {
     // theme,
      isMobile,
      userId,
      posts
    } = this.state;

    return (
      <ThemeProvider>
        <div className={clsx("section", classes.section)} id="testimonial8">
          <div className="container text-center">
            <h1 className="font-normal text-44 mt-0 text-white mx-auto mb-16">
              What our guests have to say
            </h1>
            <Carousel
              bulletColor="#bbb"
              slidesPerView={1}
              spacing={0}
              navigation={false}
              paginationClass="mt-16"
            >
              {posts.map((testimonial, index) => (
                <div className={clsx("mx-auto", classes.card)} key={index}>
                  <div
                    className={clsx({
                      "flex justify-center": true,
                      "flex-wrap": isMobile,
                    })}
                  >
                    <Avatar
                      className="w-108 h-108"
                      src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/User_with_smile.svg/1024px-User_with_smile.svg.png"
                      alt="user"
                    />
                    <p
                      className={clsx({
                        "text-white": true,
                        "text-left my-0 ml-8": !isMobile,
                        [classes.review]: true,
                      })}
                    >
                    {testimonial.message}
                    </p>
                  </div>
                  <div className="flex flex-wrap mt-4 justify-center mb-2">
                    {[1, 2, 3, 4, 5].map((i) => (
                      <Icon
                        key={i}
                        className="mx-1"
                        fontSize="small"
                        color="secondary"
                      >
                        start
                      </Icon>
                    ))}
                  </div>
                  <h5 className="inline-block m-0 font-medium text-white">
                    {testimonial.username}
                  </h5>
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      </ThemeProvider>
    );
  };

  
}

export default withStyles(useStyles)(Testimonial8);