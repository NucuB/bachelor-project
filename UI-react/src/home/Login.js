// import React from "react";
// import Footer2 from "./sections/Footer2";
// import axios from 'axios'

// import { Card, Grid, TextField, Fab, Icon } from "@material-ui/core";

// class Login extends React.Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       username: '',
//       password: ''
//     }
//   }

//   componentDidMount(){
//     if(localStorage.getItem('userId')) {
//       localStorage.removeItem('userId');
//     }
//   }
//   handleLogin = () => {
//     const API_URL = 'http://localhost:8888'
//     const headers = {
//       'Content-Type': 'application/json',
//       'Authorization': '*'
//   }
//   const {username, password} = this.state;
//    const request = {
//       username:username,
//       password : password,
//     }
//     const url = API_URL + "/signin";
//     console.log(request);
//     axios.post(url, request, headers).then(response => {
//       console.log(response.data);
//       localStorage.setItem('userId', response.data);
//       window.location = "http://localhost:3000/landing9";
//     })
//     .catch(error => {
//         console.error('There was an error!', error);
//     });
//   }

//   handleChange = (event) => {
//     console.log(event.target.id);
//     console.log(event.target.value);
//     this.setState({
//       [event.target.id]: event.target.value
//     })
//   }
//   render(){
//     return (
//       <div className="container">
//           <Card elevation={0} className="p-12 border-radius-8">
//             <h4 className="font-normal m-0 mb-8">Please login!</h4>

//             <div className="flex flex-wrap items-center ml--2">
//               <TextField id="username" onChange={this.handleChange} variant="outlined" label="Username" className="m-2" value={this.props.username} />
//               <TextField id="password" onChange={this.handleChange} variant="outlined" label="Password" className="m-2" value={this.props.password} />

//               <div>
//               <Fab onClick={() => this.handleLogin()} variant="round" color="primary" size="medium" className="m-2">
//                   <Icon>navigate_next</Icon>
//               </Fab>
//             </div>
//             </div>
//           </Card>
//         </div>
//     );
//   }
// };

// export default Login;







import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Card, Grid, TextField, Fab, Icon } from "@material-ui/core";
import axios from 'axios';
import { Button } from "@material-ui/core";
import './Login.css';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';

let errors = {};

class Login extends Component {
  state = {
    username: '',
    password: '',
    isError: false,
  };

  componentDidMount() {
    if (localStorage.getItem('userId')) {
      localStorage.removeItem('userId');
    }
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleLogin();
    }
  }

  handleLogin = () => {
    const API_URL = 'http://localhost:8888'
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': '*'
    }
    const { username, password } = this.state;
    const request = {
      username: username,
      password: password,
    }
    const url = API_URL + "/signin";
    console.log(request);
    axios.post(url, request, headers).then(response => {
      console.log(response.data);
      localStorage.setItem('userId', response.data);
      window.location = "http://localhost:3000/homepage";
      alert('Login successful');
      errors['WrongCredentials'] = null;
      this.setState({ isError: false });
    })
      .catch(error => {
        // if (error.status === 400) {
        //   console.log(
        //     'Your Username or Password is incorrect. Please try again!'
        //   );
        //   errors['WrongCredentials'] =
        //     'Your Username or Password is incorrect. Please try again!';
        //   this.setState({ isError: true });
        // } else 
        if (error.status == 403) {
          console.log(
            'Your Username or Password is incorrect. Please try again!'
          );
          errors['WrongCredentials'] =
            'Your Username or Password is incorrect. Please try again!';
          this.setState({ isError: true });
        } else {
          console.log(
            'Your Username or Password is incorrect. Please try again!'
          );
          errors['WrongCredentials'] =
            'Your Username or Password is incorrect. Please try again!';
          this.setState({ isError: true });
        }
      });
  };

  onChangeUsername = event =>
    this.setState({ username: event.target.value });
  onChangePassword = event => this.setState({ password: event.target.value });

  render() {
    const { username, password } = this.state;
    return (
      <div>
        <div className="container-fluid mt-24">
          <Card elevation={3} className="card p-25 login border-radius-8">
            <h1>Hotel Booking System</h1>
            <h4 className="mb-2 text-white">Sign in</h4>
            <div align="center">
              <div className="card-body mx-auto">
                <div className="form-group form-row">
                  <div className="col">
                    <TextField id="username" InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <AccountCircle />
                        </InputAdornment>
                      ),
                    }} onChange={this.onChangeUsername} margin="dense" variant="outlined" className="m-2" placeholder="Username" value={username} />
                  </div>
                  <div className="col">
                    <TextField input InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <VpnKeyIcon />
                        </InputAdornment>
                      ),
                    }} type="password" id="password" onChange={this.onChangePassword} margin="dense"
                      variant="outlined" className="m-2" placeholder="Password" value={password} />
                  </div>
                  <Fab onClick={() => this.handleLogin()} variant="extended" color="primary" size="medium" className="m-2">
                    <Icon>navigate_next</Icon>
                  </Fab>
                </div>
                <div>
                  <span style={{ color: 'red' }}>{errors['WrongCredentials']}</span>
                </div>
                <div className="flex flex-wrap">

                  <Button onClick={() => window.location = "http://localhost:3000/homepage"}
                    variant="contained"
                    color="primary"
                    className="mr-4 border-radius-8"
                  >
                    Homepage
              </Button>
                </div>
              </div>
            </div>
          </Card>
        </div >
      </div>
    );
  }
}
export default Login;


