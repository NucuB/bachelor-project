import React from "react";
import Footer2 from "./sections/Footer2";
import axios from 'axios';
import "./Register.css";
import { Card, Grid, TextField, Fab, Icon } from "@material-ui/core";

class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      firstname: '',
      lastname: '',
      password: '',
      email: '',
      passwordconfirmed: '',
    }
  }

  handleRegistration = () => {
    const API_URL = 'http://localhost:8888'
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': '*'
    }
    const { username, firstname, lastname, email, password, passwordconfirmed } = this.state;
    const request = {
      username: username,
      firstName: firstname,
      lastName: lastname,
      email: email,
      password: password,
      passwordConfirmed: passwordconfirmed,
    }
    const url = API_URL + "/signup";
    console.log(request);
    if (password.length < 5) {
      alert('Password requires minimum 5 characters');
    } else {
      axios.post(url, request, headers).then(response => { alert('You have successfully registered'); window.location = "http://localhost:3000/login"; })
        .catch(error => {
          console.error('There was an error!', error);
        });
    }

  }

  handleChange = (event) => {

    this.setState({
      [event.target.id]: event.target.value
    })
  }

  render() {
    return (
      <div className="container-fluid mt-24">
        <Card elevation={0} className="card p-12 login border-radius-8">
          <h1>Hotel Booking System</h1>
          <h4 className="m-0 mb-8 text-white">Sign Up</h4>
          <div align="center" className="card-body mx-auto form-group ml-2">
            <div className="form-row">
              <div className="col">
                <TextField id="username" onChange={this.handleChange} margin="dense" variant="outlined" label="Username" className="m-2" value={this.props.username} />
              </div>
              <div className="col">
                <TextField id="firstname" onChange={this.handleChange} margin="dense" variant="outlined" label="First Name" className="m-2" value={this.props.firstname} />
              </div>
              <div className="col">
                <TextField id="lastname" onChange={this.handleChange} margin="dense" variant="outlined" label="Last Name" className="m-2" value={this.props.lastname} />
              </div>
              <div className="col">
                <TextField id="email" onChange={this.handleChange} margin="dense" variant="outlined" label="Email" className="m-2" value={this.props.email} />
              </div>
              <div className="col">
                <TextField input type="password" id="password" onChange={this.handleChange} margin="dense" variant="outlined" label="Password" className="m-2" value={this.props.password} />
              </div>
              <div>
                <Fab onClick={() => this.handleRegistration()} variant="round" color="primary" size="medium" className="m-2">
                  <Icon>navigate_next</Icon>
                </Fab>
              </div>
            </div>
          </div>
        </Card>
      </div>
    );
  }
};

export default Register;
