import React from "react";
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import "react-perfect-scrollbar/dist/css/styles.css";
import { MuiThemeProvider } from "@material-ui/core/styles";
import Scrollbar from "react-perfect-scrollbar";
import { Theme } from "./theme";
import GlobalCss from "./styles/jss/GlobalCss";

import ReservationsLanding from "./home/ReservationsLanding";
import Login from "./home/Login.js";
import Register from "./home/Register.js";
import Homepage from "./home/Homepage";

function App() {
  return (
    <MuiThemeProvider theme={Theme}>
      <GlobalCss>
        <Scrollbar
          className="h-full-screen scrollable-content"
          option={{ suppressScrollX: true }}
        >
          <Router basename="/">
            <Switch>
              <Route path="/homepage" component={Homepage} exact/>
              <Route path="/reservations" component={ReservationsLanding} />
              <Route path="/login" component={Login} />
              <Route path="/signup" component={Register} />
              <Redirect path="/" exact to="homepage" />
              {/* <Route component={Error} /> */}
            </Switch>
          </Router>
        </Scrollbar>
      </GlobalCss>
    </MuiThemeProvider>
  );
}

export default App;
