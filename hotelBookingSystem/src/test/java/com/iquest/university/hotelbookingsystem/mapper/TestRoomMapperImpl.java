package com.iquest.university.hotelbookingsystem.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.RoomMapper;
import com.iquest.university.mapper.RoomMapperImpl;
import com.iquest.university.model.Room;
import com.iquest.university.utility.Currency;

public class TestRoomMapperImpl {

	private RoomMapper roomMapper;

	@BeforeEach
	public void setUp() {
		roomMapper = new RoomMapperImpl();
	}

	@Test
	public void testReturnedDTOFromMapperShouldBeSameAsEntity() {
		Room actualRoom = new Room(1, 20, true, true, true, true);
		actualRoom.setCurrency(Currency.EURO);
		RoomDTO expectedRoomDTO = roomMapper.toDto(actualRoom);

		assertEquals(expectedRoomDTO.getPrice(), actualRoom.getPrice());
		assertEquals(expectedRoomDTO.getCurrency(), actualRoom.getCurrency());

	}

	@Test
	public void testReturnedEntityFromMapperShouldBeSameAsDTO() {
		RoomDTO actualRoomDTO = new RoomDTO();
		actualRoomDTO.setBusy(false);
		actualRoomDTO.setCapacity(1);
		actualRoomDTO.setPrice(20);

		Room expectedRoom = roomMapper.toEntity(actualRoomDTO);
		assertEquals(expectedRoom.getPrice(), actualRoomDTO.getPrice());
		assertEquals(expectedRoom.getCurrency(), actualRoomDTO.getCurrency());
	}

	@Test
	public void testEntityNullAsParameterShouldThrowException() {
		Room nullRoom = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			roomMapper.toDto(nullRoom);
		});
	}

	@Test
	public void testDTONullAsParameterShouldThrowException() {
		RoomDTO nullRoomDTO = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			roomMapper.toEntity(nullRoomDTO);
		});
	}
}
