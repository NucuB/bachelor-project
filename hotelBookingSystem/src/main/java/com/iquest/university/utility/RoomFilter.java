package com.iquest.university.utility;

public class RoomFilter extends Filter {

	public RoomFilter(Integer capacity, boolean busy, Integer id) {
		if (capacity != null) {
			this.save("capacity", capacity);
		} else if (id != null) {
			this.save("id", id);
		} else {
			this.save("busy", busy);
		}
	}

	@Override
	protected void validate() {
		// TODO Auto-generated method stub
		
	}
}
