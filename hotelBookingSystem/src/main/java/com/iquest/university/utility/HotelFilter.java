package com.iquest.university.utility;

public class HotelFilter extends Filter {

	public HotelFilter(String town, String name, Luxury luxury) {

		if (town != null) {
			this.save("town", town);
		} else if (name != null) {
			this.save("name", name);
		} else if (luxury != null) {
			this.save("luxury", luxury);
		}
	}

	@Override
	protected void validate() {
		
		//Aici folosesc setteri si getteri, verificand parametrii primiti si construiesc filtrul
	}
	
	public void setTown(String town) {
		this.save("town", town);
	}

}
