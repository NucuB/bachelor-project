package com.iquest.university.utility;

import java.util.HashMap;
import java.util.Map;

public abstract class Filter {

	private Map<String, Object> filterMap;

	public Filter() {
		this.filterMap = new HashMap<String, Object>();
	}

	public void save(String key, Object object) {
		filterMap.put(key, object);
	}

	public Object get(String field) {
		return filterMap.get(field);
	}
	
	protected abstract void validate();
	
	public Filter build() {
		this.validate();
		return this;
	}

}
