package com.iquest.university.dto;

public class UserDTO {

	private long userId;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String username;
	private String passwordConfirm;

	public UserDTO(String firstName2, String lastName2, String email2, String username2, String password2,
			String passwordConfirmed) {
				this.username = username2;
				this.firstname = firstName2;
				this.lastname = lastName2;
				this.email = email2;
				this.password = password2;
				this.passwordConfirm = passwordConfirmed;
	}

	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

}
