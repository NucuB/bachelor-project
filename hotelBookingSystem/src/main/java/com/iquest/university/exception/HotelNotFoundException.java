package com.iquest.university.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class HotelNotFoundException extends EntityNotFoundException {

	public HotelNotFoundException() {
		super("Hotel was not found");
	}
}
