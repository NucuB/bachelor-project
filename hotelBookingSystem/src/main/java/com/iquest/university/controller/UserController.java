package com.iquest.university.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.service.UserService;

@RestController
@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:3001", "http://localhost:3002" })
@RequestMapping(value = "/users", produces = "application/json")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, value = "lastname")
	public Collection<UserDTO> getUsers(@RequestParam(name = "lastname") String lastname) {
		return userService.retrieveAllUsers(lastname);
	}

	@RequestMapping(method = RequestMethod.GET)
	public UserDTO findUser(@RequestParam(name = "id") long userId) {
		return userService.findUser(userId);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public UserDTO createUser(@RequestBody UserDTO dto) {
		return userService.saveUser(dto);
	}

	

}
