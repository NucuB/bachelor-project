package com.iquest.university.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.model.User;
import com.iquest.university.payload.ApiResponse;
import com.iquest.university.payload.LoginRequest;
import com.iquest.university.payload.SignUpRequest;
import com.iquest.university.service.SecurityService;
//import com.iquest.university.service.SecurityService;
import com.iquest.university.service.UserService;
import com.iquest.university.utility.UserValidator;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:3001", "http://localhost:3002" })
@Controller
public class AuthController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private SecurityService securityService;
	
//	@Autowired
//	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
    AuthenticationManager authenticationManager;

	@Autowired
	private UserValidator userValidator;

	@RequestMapping(method = RequestMethod.GET, value = "/reg")
	public String viewRegister(Model model) {
		model.addAttribute("userDTO", new UserDTO());
		return "reg";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/signup")
    public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest) {
        UserDTO userDTO = new UserDTO(signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getEmail(),
        		signUpRequest.getUsername(), signUpRequest.getPassword(), signUpRequest.getPasswordConfirmed());

        UserDTO dto = userService.saveTry(userDTO);
        securityService.autoLogin(signUpRequest.getUsername(), signUpRequest.getPassword());
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(dto.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
		
		UserDTO user = userService.findByUsername(loginRequest.getUsername());
		System.out.println(user.getUserId());
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        return ResponseEntity.ok(user.getUserId());
		 
    }
	
	@RequestMapping(value =  "/welcome", method = RequestMethod.GET)
	public String welcome(Model model) {
		return "welcome";
	}

}
