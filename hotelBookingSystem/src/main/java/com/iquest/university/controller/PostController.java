package com.iquest.university.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.model.Post;
import com.iquest.university.payload.PostResponse;
import com.iquest.university.service.PostService;
import com.iquest.university.service.ReservationService;

@RestController
@RequestMapping(value = "/users", produces = "application/json")
@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:3001", "http://localhost:3002" })
public class PostController {

	

	@Autowired
	private PostService postService;

	@RequestMapping(method = RequestMethod.GET, value = "{userId}/posts")
	public List<PostResponse> getPosts(@PathVariable(name = "userId") long userId) {
		return postService.retrieveAllPosts(userId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{userId}/posts", consumes = "application/json")
	public Post createPost(@RequestBody Post post, @PathVariable(name = "userId") long userId) {
		return postService.addPost(post);
	}
}
