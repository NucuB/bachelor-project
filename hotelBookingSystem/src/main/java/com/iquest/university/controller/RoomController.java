package com.iquest.university.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.service.RoomService;
import com.iquest.university.utility.Filter;
import com.iquest.university.utility.RoomFilter;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:3001", "http://localhost:3002" })
@RestController
@RequestMapping(value = "/rooms", produces = "application/json")
public class RoomController {

	@Autowired
	private RoomService roomService;

	@RequestMapping(method = RequestMethod.GET)
	public Collection<RoomDTO> getRooms(@RequestParam(name = "capacity", required = false) Integer capacity,
			@RequestParam(name = "busy", defaultValue = "false") boolean busy,
			@RequestParam(name = "id", required = false) Integer id) {
		Filter roomFilter = new RoomFilter(capacity, busy, id);
		return roomService.retrieveAllRooms(roomFilter);
	}

//	@RequestMapping(method = RequestMethod.GET, value = "busy")
//	public Collection<RoomDTO> getRooms(@RequestParam(name = "busy") boolean busy) {
//		return roomService.retrieveAllRooms(busy);
//	}
//
//	@RequestMapping(method = RequestMethod.GET)
//	public RoomDTO findRoom(@RequestParam(name = "id") long roomId) {
//		return roomService.findRoom(roomId);
//	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public RoomDTO createRoom(@RequestBody RoomDTO dto) {
		return roomService.saveRoom(dto);
	}

}
