//package com.iquest.university.repository;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import javax.persistence.EntityManager;
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Predicate;
//import javax.persistence.criteria.Root;
//
//import com.iquest.university.model.Hotel;
//import com.iquest.university.utility.Filter;
//import com.iquest.university.utility.HotelFilter;
//import com.mysql.cj.util.StringUtils;
//
//public class HotelRepositoryCustomImpl implements HotelRepositoryCustom {
//
//	private EntityManager entityManager;
//
//	public HotelRepositoryCustomImpl(EntityManager em) {
//		this.entityManager = em;
//	}
//
////	@Override
////	public Collection<Hotel> getAllHotelsByFilter(HotelFilter filter) {
////		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
////		CriteriaQuery<Hotel> criteriaQuery = criteriaBuilder.createQuery(Hotel.class);
////		Root<Hotel> hotel = criteriaQuery.from(Hotel.class);
////		List<Predicate> predicates = new ArrayList<>();
////
////		if (!StringUtils.isEmptyOrWhitespaceOnly(filter.getHotelName())
////				&& !StringUtils.isEmptyOrWhitespaceOnly(filter.getTown())) {
////			
////			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(hotel.get("name"), filter.getHotelName()),
////					(criteriaBuilder.equal(hotel.get("town"), filter.getTown()))));
////		} else if (filter.getLuxury() != null) {
////			predicates.add(criteriaBuilder.equal(hotel.get("luxury"), filter.getLuxury()));
////		} else if (!StringUtils.isEmptyOrWhitespaceOnly(filter.getTown())) {
////			predicates.add(criteriaBuilder.equal(hotel.get("town"), filter.getTown()));
////		} else if (!StringUtils.isEmptyOrWhitespaceOnly(filter.getHotelName())) {
////			predicates.add(criteriaBuilder.equal(hotel.get("name"), filter.getHotelName()));
////		}
////		criteriaQuery.where(predicates.toArray(new Predicate[0]));
////		return entityManager.createQuery(criteriaQuery).getResultList();
////	}
//	
//	@Override
//	public Collection<Hotel> getAllHotelsByFilter(Filter filter) {
//		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Hotel> criteriaQuery = criteriaBuilder.createQuery(Hotel.class);
//		Root<Hotel> hotel = criteriaQuery.from(Hotel.class);
//		List<Predicate> predicates = new ArrayList<>();
//
//		if ((filter.get("name") != null && filter.get("town") != null)) {
//			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(hotel.get("name"), filter.get("name")),
//					(criteriaBuilder.equal(hotel.get("town"), filter.get("town")))));
//		} else if (filter.get("luxury") != null) {
//			predicates.add(criteriaBuilder.equal(hotel.get("luxury"), filter.get("luxury")));
//		} else if (filter.get("town") != null) {
//			predicates.add(criteriaBuilder.equal(hotel.get("town"), filter.get("town")));
//		} else if (filter.get("name") != null) {
//			predicates.add(criteriaBuilder.equal(hotel.get("name"), filter.get("name")));
//		}
//		criteriaQuery.where(predicates.toArray(new Predicate[0]));
//		return entityManager.createQuery(criteriaQuery).getResultList();
//	}
//
//}
