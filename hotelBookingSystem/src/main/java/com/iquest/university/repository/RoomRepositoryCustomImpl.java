package com.iquest.university.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.iquest.university.model.Room;
import com.iquest.university.utility.Filter;

public class RoomRepositoryCustomImpl implements RoomRepositoryCustom {

	private EntityManager entityManager;

	public RoomRepositoryCustomImpl(EntityManager em) {
		this.entityManager = em;
	}

	@Override
	public Collection<Room> getAllRoomsByFilter(Filter filter) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Room> criteriaQuery = criteriaBuilder.createQuery(Room.class);
		Root<Room> room = criteriaQuery.from(Room.class);
		List<Predicate> predicates = new ArrayList<>();

		if ((filter.get("capacity") != null && filter.get("busy") != null)) { 
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(room.get("capcity"), filter.get("capacity")),
					(criteriaBuilder.equal(room.get("busy"), filter.get("busy")))));
		} //else if (filter.get("))
		if (filter.get("capacity") != null) {
			predicates.add(criteriaBuilder.equal(room.get("capacity"), filter.get("capacity")));
		} else if (filter.get("id") != null) {
			predicates.add(criteriaBuilder.equal(room.get("roomId"), filter.get("id")));
		} else {
			predicates.add(criteriaBuilder.equal(room.get("busy"), filter.get("busy")));
		}
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

}
