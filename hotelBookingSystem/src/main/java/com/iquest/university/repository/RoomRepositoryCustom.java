package com.iquest.university.repository;

import java.util.Collection;

import com.iquest.university.model.Room;
import com.iquest.university.utility.Filter;

public interface RoomRepositoryCustom {

	Collection<Room> getAllRoomsByFilter(Filter filter);
}
