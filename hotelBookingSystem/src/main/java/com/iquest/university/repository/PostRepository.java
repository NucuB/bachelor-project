package com.iquest.university.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iquest.university.model.Post;
import com.iquest.university.model.Reservation;

@Repository
public interface PostRepository extends CrudRepository<Post, Long>{

	
	Collection<Post> findAllByUserId(long id);
}
