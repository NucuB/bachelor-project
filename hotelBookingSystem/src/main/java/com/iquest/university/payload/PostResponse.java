package com.iquest.university.payload;

public class PostResponse {

	private String message;
	private String username;
	
	

	public PostResponse(String message, String username) {
		super();
		this.message = message;
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
