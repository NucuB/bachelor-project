package com.iquest.university.service;

import java.util.Collection;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.model.User;

public interface UserService {

	Collection<UserDTO> retrieveAllUsers(String lastname);

	UserDTO findUser(long id);

	UserDTO saveUser(UserDTO user);
	
	UserDTO deleteUser(long id);
	
    UserDTO findByUsername(String username);
    
   // void saveTry(User user);

	UserDTO saveTry(UserDTO userDTO);
}
