package com.iquest.university.service;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public interface SecurityService {
	
	String findLoggedInUsername();
	
	void autoLogin(String username, String password);
	//UsernamePasswordAuthenticationToken autoLogin(String username, String password);
}
