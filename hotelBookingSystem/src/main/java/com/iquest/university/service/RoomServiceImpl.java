package com.iquest.university.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.exception.RoomNotFoundException;
import com.iquest.university.mapper.RoomMapper;
import com.iquest.university.model.Room;
import com.iquest.university.repository.RoomRepository;
import com.iquest.university.utility.Filter;

@Service
public class RoomServiceImpl implements RoomService {

	private RoomRepository roomRepository;
	private RoomMapper roomMapper;

	public RoomServiceImpl(RoomRepository roomRepository, RoomMapper roomMapper) {
		this.roomMapper = roomMapper;
		this.roomRepository = roomRepository;
	}

//	@Override
//	public RoomDTO findRoom(long id) {
//		if (id == 0) {
//			throw new InvalidArgumentException("Please insert a valid ID");
//		}
//		return roomMapper.toDto(roomRepository.findByRoomId(id).orElseThrow(() -> new RoomNotFoundException()));
//	}
//
//	@Override
//	public Collection<RoomDTO> retrieveAllRooms(int capacity) {
//		if (capacity == 0) {
//			throw new InvalidArgumentException("Please insert a valid capacity");
//		}
//		return roomRepository.findAllByCapacity(capacity).stream()
//												  		 .map(roomMapper::toDto)
//												  		 .collect(Collectors.toList());
//	}
//
//	@Override
//	public Collection<RoomDTO> retrieveAllRooms(boolean busy) {
//		return roomRepository.findAllByBusy(busy).stream()
//												 .map(roomMapper::toDto)
//												 .collect(Collectors.toList());
//	}

	@Override
	public RoomDTO deleteRoom(long id) {
		if (id == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		return roomMapper.toDto(roomRepository.deleteByRoomId(id).orElseThrow(() -> new RoomNotFoundException()));
	}

	@Override
	public RoomDTO saveRoom(RoomDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid room");
		}
		Room room = roomMapper.toEntity(dto);
		return roomMapper.toDto(roomRepository.save(room));
	}

	@Override
	public Collection<RoomDTO> retrieveAllRooms(Filter filter) {
		return roomRepository.getAllRoomsByFilter(filter).stream()
														 .map(roomMapper::toDto)
														 .collect(Collectors.toList());
	}

}
