package com.iquest.university.service;

import java.util.Collection;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.utility.Filter;

public interface RoomService {

	Collection<RoomDTO> retrieveAllRooms(Filter filter);
	
//	Collection<RoomDTO> retrieveAllRooms(boolean busy);

//	RoomDTO findRoom(long id);

	RoomDTO saveRoom(RoomDTO dto);
	
	RoomDTO deleteRoom(long id);
}
