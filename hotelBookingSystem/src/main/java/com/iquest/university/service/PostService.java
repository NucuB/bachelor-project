package com.iquest.university.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.exception.UserNotFoundException;
import com.iquest.university.mapper.ReservationMapper;
import com.iquest.university.model.Post;
import com.iquest.university.model.User;
import com.iquest.university.payload.PostResponse;
import com.iquest.university.repository.PostRepository;
import com.iquest.university.repository.ReservationRepository;
import com.iquest.university.repository.RoomRepository;
import com.iquest.university.repository.UserRepository;

@Service
public class PostService {

	private UserRepository userRepository;
	private PostRepository postRepository;

	public PostService(PostRepository postRepository, UserRepository userRepository) {
		this.postRepository = postRepository;
		this.userRepository = userRepository;
	}
	
	
	
	public Post addPost(Post post) {
		//System.out.println(post.getUser());
		//User userFromPost = userRepository.findById(post.getUserId())
		Optional<User> user = userRepository.findById(post.getUserId());
		post.setUser(user.get());
		return postRepository.save(post);
		
		
	}


	public List<PostResponse> retrieveAllPosts(long userId) {
		if (userId == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		//var user = userRepository.findByUserId(userId).orElseThrow(() -> new UserNotFoundException());
//		return postRepository.findAllByUserUserId(user.getUserId()).stream()
//																		  .collect(Collectors.toList());
		List<Post> posts = (List<Post>) postRepository.findAllByUserId(userId);
		List<PostResponse> listOfPosts = new ArrayList<>();
		for (Post post : posts) {
			PostResponse postResponse = new PostResponse(post.getMessage(), post.getUser().getUsername());
			listOfPosts.add(postResponse);
		}
		
		return listOfPosts;
		
	}
}
