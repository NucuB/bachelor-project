# README #

## Hotel Booking System ##

This is my Bachelor Project, represented by a booking system for hotel's rooms.

## Scope ##

The application was designed for making booking easier than it is. The friendly UI should guide every kind of user in order to make a successful rooms reservation.

### Main Functionalities ###

* Login/Register
* CRUD operations
* Reserve any room at choice
* Room Filter
* Leave posts/comments
* Linking between UI components: scrollTo(instead of link references) 

### Development ###

* Backend development: Java, Spring Boot
* Frontend development: ReactJS
